maintainer="fossdd <fossdd@pwned.life>"
pkgname=sdl2-compat
pkgver=2.30.52
pkgrel=1
pkgdesc="SDL2 compatibility layer that uses SDL3 behind the scenes"
url="https://github.com/libsdl-org/sdl2-compat"
arch="all"
license="Zlib"
depends="sdl3"
makedepends="
	cmake
	samurai
	sdl3-dev
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/libsdl-org/sdl2-compat/releases/download/release-$pkgver/sdl2-compat-$pkgver.tar.gz"
options="!check" # flaky

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=ON \
		-DCMAKE_BUILD_TYPE=None \
		$crossopts
	cmake --build build
}

check() {
	ctest --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
923c7c8c2a2bc4c0cfb1de2c48af0bdee672576b84f647c316e5f60394ec553ed0db699d3af5047fb48441b03918a39c1dc02fe0c424d0b382b385f58f3b2d6f  sdl2-compat-2.30.52.tar.gz
"
